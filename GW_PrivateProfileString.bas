Attribute VB_Name = "GW_PPS"
Option Explicit

Private Declare Function WritePrivateProfileString _
    Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpString As Any, _
        ByVal lpFileName As String) As Long

Private Declare Function GetPrivateProfileString _
    Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpDefault As String, _
        ByVal lpReturnedString As String, _
        ByVal nSize As Long, _
        ByVal lpFileName As String) As Long


'保存するデータのサイズ
Const Save_File_Title As String = "data.ini"

Const String_Size As Integer = 100

Const LP_APP_NAME = "PROGRAM88"
Const KEY_PTLSTCNT = "PATTERNLISTCOUNT"
Const KEY_PTLST = "PATTERNLIST"
Const KEY_FVLSTCNT = "FAVORITESLISTCOUNT"
Const KEY_FVLST = "FAVORITESLIST"

'保存するファイル名
Dim Init_File_Path As String

Public Sub InitPPS()
    Dim p As String
    
    p = App.Path
    If Right$(p, 1) <> "\" Then
        p = p & "\"
    End If
    
    Init_File_Path = p & Save_File_Title
    
End Sub

'文字列から余計な文字(Asciiコード＝０)を削除する
Function Trim0(s As String) As String
  Dim tmp As String, i As Integer
  
  tmp = ""
  For i = 1 To Len(s)
    If Asc(Mid(s, i, 1)) = 0 Then
      Exit For
    End If
    tmp = tmp & Mid(s, i, 1)
  Next i
  
  Trim0 = tmp
  
End Function

'設定ファイルへ書き込む
Function WPPS( _
          lpApplicationName0 As String, _
          lpKeyName0 As String, _
          lpString0 As String) As Long
  
  WPPS = WritePrivateProfileString( _
                  lpApplicationName0, _
                  lpKeyName0, _
                  lpString0, _
                  Init_File_Path)
        
End Function
        
'設定ファイルから読み出す
Function GPPS( _
            lpApplicationName0 As String, _
            lpKeyName0 As String, _
            lpDefault0 As String, _
            lpReturnedString0 As String) As Long
  
  Dim tmp As String * String_Size
  
  GPPS = GetPrivateProfileString( _
                lpApplicationName0, _
                lpKeyName0, _
                lpDefault0, _
                tmp, _
                String_Size, _
                Init_File_Path)
                
  lpReturnedString0 = Trim0(tmp)
  
End Function
        
'保存されているコンボボックスのパターンリストの数を返す
Public Function getPtnListCount() As Integer
    Dim s As String
    
    GPPS LP_APP_NAME, KEY_PTLSTCNT, "0", s
    
    getPtnListCount = CInt(Val(s))
    
End Function

'コンボボックスのパターンリストの数を保存する
Public Sub setPtnListCount(ct As Integer)
    Dim s As String
    
    s = Format(ct)
    
    WPPS LP_APP_NAME, KEY_PTLSTCNT, s

End Sub

'保存されているコンボボックスのパターンリストの指定番号項目を読み出す
Public Function getPtnList(n As Integer) As String
    Dim s As String, ky As String
    
    ky = KEY_PTLST & Format(n)
    
    GPPS LP_APP_NAME, ky, " ", s
    
    getPtnList = Trim(s)

End Function

'コンボボックスのパターンリストの指定番号の項目を保存する
Public Sub setPtnList(n As Integer, strPtnList As String)
    Dim ky As String
    
    ky = KEY_PTLST & Format(n)
    
    WPPS LP_APP_NAME, ky, strPtnList
    
End Sub

'保存されているお気に入りのリストの数を読み出す
Public Function getFvListCount() As Integer
    Dim s As String
    
    GPPS LP_APP_NAME, KEY_FVLSTCNT, "0", s
    
    getFvListCount = CInt(Val(s))

End Function

'お気に入りのリストの数を保存する
Public Sub setFvListCount(ct As Integer)
    Dim s As String
    
    s = Format(ct)
    
    WPPS LP_APP_NAME, KEY_FVLSTCNT, s
End Sub

'保存されているお気に入りのリストの番号指定項目を読み出す
Public Function getFvList(n As Integer) As String
    Dim s As String, ky As String
    
    ky = KEY_FVLST & Format(n)
    
    GPPS LP_APP_NAME, ky, " ", s
    
    getFvList = Trim(s)
    
    Debug.Print Trim(s)

End Function

'お気に入りのリストを指定番号の項目へ保存する
Public Sub setFvList(n As Integer, strFvList As String)
    Dim ky As String
    
    ky = KEY_FVLST & Format(n)
    
    WPPS LP_APP_NAME, ky, strFvList

End Sub
