VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ファイル名一括変更"
   ClientHeight    =   5715
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   10260
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5715
   ScaleWidth      =   10260
   StartUpPosition =   2  '画面の中央
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   50
      TabIndex        =   3
      Text            =   "*.*"
      Top             =   5350
      Width           =   2235
   End
   Begin VB.ListBox List1 
      Height          =   240
      Left            =   3120
      TabIndex        =   25
      Top             =   5055
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame Frame1 
      Caption         =   "名前変更規則"
      Height          =   1900
      Left            =   3735
      TabIndex        =   19
      Top             =   150
      Width           =   6450
      Begin VB.CommandButton Command9 
         Caption         =   "元に戻す"
         Enabled         =   0   'False
         Height          =   330
         Left            =   5350
         TabIndex        =   18
         Top             =   1450
         Width           =   915
      End
      Begin VB.CommandButton Command8 
         Caption         =   "チェック"
         Height          =   330
         Left            =   3350
         TabIndex        =   16
         Top             =   1450
         Width           =   915
      End
      Begin VB.TextBox Text6 
         Height          =   300
         Left            =   1905
         TabIndex        =   14
         Text            =   "*"
         Top             =   1035
         Width           =   510
      End
      Begin VB.TextBox Text5 
         Height          =   300
         Left            =   1905
         TabIndex        =   13
         Text            =   "*"
         Top             =   300
         Width           =   510
      End
      Begin VB.TextBox Text4 
         Height          =   315
         IMEMode         =   2  'ｵﾌ
         Left            =   1500
         TabIndex        =   15
         Text            =   "1"
         Top             =   1500
         Width           =   585
      End
      Begin VB.CommandButton Command7 
         Caption         =   "変更開始"
         Enabled         =   0   'False
         Height          =   330
         Left            =   4350
         TabIndex        =   17
         Top             =   1450
         Width           =   915
      End
      Begin VB.TextBox Text3 
         Height          =   300
         Left            =   135
         TabIndex        =   12
         Text            =   "*"
         Top             =   1035
         Width           =   1545
      End
      Begin VB.TextBox Text2 
         Height          =   300
         Left            =   135
         TabIndex        =   11
         Text            =   "*"
         Top             =   300
         Width           =   1545
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "．"
         Height          =   180
         Left            =   1725
         TabIndex        =   24
         Top             =   1125
         Width           =   120
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "．"
         Height          =   180
         Left            =   1725
         TabIndex        =   23
         Top             =   390
         Width           =   120
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "開始番号"
         Height          =   180
         Left            =   650
         TabIndex        =   22
         Top             =   1550
         Width           =   720
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  '実線
         Caption         =   "説明"
         Height          =   1080
         Left            =   2535
         TabIndex        =   21
         Top             =   285
         Width           =   3705
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "↓"
         Height          =   180
         Left            =   1080
         TabIndex        =   20
         Top             =   735
         Width           =   180
      End
   End
   Begin VB.CommandButton Command6 
      Caption         =   "反映"
      Height          =   285
      Left            =   2350
      TabIndex        =   4
      Top             =   5350
      Width           =   650
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3500
      Left            =   3750
      TabIndex        =   6
      Top             =   2145
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   6165
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      OLEDropMode     =   1
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      OLEDropMode     =   1
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Title"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Type"
         Object.Width           =   1147
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Path"
         Object.Width           =   10583
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Date"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Size"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "After"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "er"
         Object.Width           =   706
      EndProperty
   End
   Begin VB.CommandButton Command5 
      Caption         =   "↓"
      Height          =   380
      Left            =   3100
      TabIndex        =   10
      Top             =   4550
      Width           =   600
   End
   Begin VB.CommandButton Command4 
      Caption         =   "↑"
      Height          =   380
      Left            =   3100
      TabIndex        =   9
      Top             =   4100
      Width           =   600
   End
   Begin VB.CommandButton Command3 
      Caption         =   "CLR"
      Height          =   380
      Left            =   3100
      TabIndex        =   8
      Top             =   3400
      Width           =   600
   End
   Begin VB.CommandButton Command2 
      Caption         =   "DEL"
      Height          =   380
      Left            =   3100
      TabIndex        =   7
      Top             =   2950
      Width           =   600
   End
   Begin VB.CommandButton Command1 
      Caption         =   ">>"
      Height          =   380
      Left            =   3100
      TabIndex        =   5
      Top             =   2500
      Width           =   600
   End
   Begin VB.FileListBox File1 
      Height          =   3150
      Left            =   50
      MultiSelect     =   2  '拡張
      OLEDropMode     =   1  '手動
      TabIndex        =   2
      Top             =   2150
      Width           =   3000
   End
   Begin VB.DirListBox Dir1 
      Height          =   1560
      Left            =   50
      OLEDropMode     =   1  '手動
      TabIndex        =   1
      Top             =   500
      Width           =   3000
   End
   Begin VB.DriveListBox Drive1 
      Height          =   300
      Left            =   50
      TabIndex        =   0
      Top             =   100
      Width           =   3000
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000016&
      X1              =   0
      X2              =   3615
      Y1              =   15
      Y2              =   15
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   3525
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー(&M)"
      Begin VB.Menu mnuFavorites 
         Caption         =   "お気に入り(&F)"
         Begin VB.Menu mnuFvList 
            Caption         =   "(ありません)"
            Enabled         =   0   'False
            Index           =   0
         End
         Begin VB.Menu mnuBar 
            Caption         =   "-"
         End
         Begin VB.Menu mnuAddFvList 
            Caption         =   "お気に入りへ追加(&A)"
         End
         Begin VB.Menu mnuBar3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuDeleteFvList 
            Caption         =   "お気に入りを削除(&D)"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuFavoritesClear 
            Caption         =   "お気に入りをすべて削除(&C)"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuReset 
         Caption         =   "リセット(&R)"
      End
      Begin VB.Menu mnuBar4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuitApplication 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'1=実際にファイル名変更する、0=ファイル名変更しない
#Const REALRUN = 1


'ListViewのSubItemの番号
Const SB_FTYPE As Integer = 1
Const SB_FPATH As Integer = 2
Const SB_FDATE As Integer = 3
Const SB_FLEN As Integer = 4
Const SB_RNAME As Integer = 5
Const SB_ERROR As Integer = 6

'ListViewのColumnHeaderの番号
Const CH_FTITLE As Integer = 1
Const CH_FTYPE As Integer = 2
Const CH_FPATH As Integer = 3
Const CH_FDATE As Integer = 4
Const CH_FLEN As Integer = 5
Const CH_RNAME As Integer = 6
Const CH_ERROR As Integer = 7


Dim StartDrive As String


'File1の選択された項目をListView1へコピーする
Private Sub File1ToListView1()
    Dim i As Integer, j As Integer, k As Integer
    Dim pth As String, s As String, s1 As String
    Dim n As Integer
    
    pth = File1.Path
    If Right$(pth, 1) <> "\" Then
        pth = pth & "\"
    End If
    
    For i = 0 To File1.ListCount - 1
        
          If File1.Selected(i) Then
            
                s = pth & File1.List(i)
                
                'リストに同じファイルがないかチェックする
                k = 0
                For j = 1 To ListView1.ListItems.Count
                    With ListView1.ListItems(j)
                        s1 = .SubItems(SB_FPATH)
                        s1 = s1 & .Text
                    End With
                    If s = s1 Then
                        k = 1
                        Exit For
                    End If
                Next j
                
                'リストに追加する
                If k = 0 Then
                    n = ListView1.ListItems.Add(, , File1.List(i)).Index
                    With ListView1.ListItems(n)
                        .SubItems(SB_FTYPE) = GetFileType(File1.List(i))
                        .SubItems(SB_FPATH) = pth
                        .SubItems(SB_FDATE) = FileDateTime(s)
                        .SubItems(SB_FLEN) = FileLen(s)
                    End With
                End If
            
          End If
        
    Next i

    ListView1.Refresh
End Sub

'名前変換のメイン処理部
'ソース条件...Src1、変更書式...Out1、ファイル名...Str1、連番の番号...n
Private Function ChangedNameSp(Src1 As String, Out1 As String, _
                                        Str1 As String, n As Integer) As String
'    Dim m As Integer, f As Integer, g As Integer
'    Dim tmp2 As String
    Dim Rst1 As String
'    Dim Org1 As String
    
    Dim i As Integer, j As Integer
    Dim p As Integer, q As Integer, k As Integer
    Dim s As String, s1 As String, s2 As String
    Dim tmp1() As String, tmp3() As String, tmp5() As String
    
    tmp1 = Wakeru(Src1)
    tmp3 = Wakeru2(Str1, tmp1)
    If tmp3(0) = "**" Then
        Rst1 = ""
    Else
        If Out1 <> "" Then
            tmp5 = Wakeru(Out1)
            p = 0
            q = 0
            For i = 0 To UBound(tmp5)
                s = Left$(tmp5(i), 1)
                If s = "*" Then
                    p = p + 1
                    tmp5(i) = tmp3(p)
                ElseIf s = "\" Then
                    k = Val(Mid$(tmp5(i), 2, 1))
                    tmp5(i) = Format$(n, String(k, "0"))
                Else
                    s1 = ""
                    For j = 1 To Len(tmp5(i))
                        s2 = Mid$(tmp5(i), j, 1)
                        If s2 = "?" Then
                            q = q + 1
                            s2 = Mid$(tmp3(0), q, 1)
                        End If
                        s1 = s1 & s2
                    Next j
                    tmp5(i) = s1
                End If
            Next i
            Rst1 = Join(tmp5, "")
        Else
            Rst1 = ""
        End If
    End If
    
    
'    Org1 = Str1
'    'ソース条件(Src1)のチェック
'    g = 0
'    If Src1 = "*" Then
'        'ソース条件が無い場合
'        g = 1
'    Else
'        'ソース条件がある場合
'        'かつ条件一致の場合 g=2
'        If (InStr(1, Src1, "*") < 1) And (InStr(1, Src1, "?") < 1) Then
'            '*,?が無いので完全一致を調べる
'            If Src1 = Org1 Then
'                g = 2
'            End If
'        Else
'            ' * や ? の条件にあうか
'        End If
'    End If
'
'    '変更書式(Out1)に従い変更
'    If (g > 0) And (Out1 = "*") Then
'          'ファイル名の変更なし
'          Rst1 = Org1
'    ElseIf g > 0 Then
'        If (InStr(1, Out1, "*") < 1) And (InStr(1, Out1, "?") < 1) Then
'            '文字だけ or 文字と連番だけのファイル名へ変更（元のファイル名を参照しない）
'            tmp2 = ""
'            s = Out1
'            m = InStr(1, s, "\")
'            Do While (m > 0)
'                f = Val(Mid$(s, m + 1, 1))
'                If m > 1 Then
'                    tmp2 = tmp2 & Left$(s, m - 1)
'                End If
'                tmp2 = tmp2 & Format(n, String(f, "0"))
'                s = Right$(s, Len(s) - (m + 2))
'                m = InStr(1, s, "\")
'            Loop
'            Rst1 = tmp2 & s
'
'        Else
'            '元のファイル名(Org1)を参照した変更
'            '*,?,\の書式に従い変更
'
'        End If
'    Else
'        'ソース条件不一致(g=0)
'        Rst1 = ""
'    End If

    ChangedNameSp = Rst1
    
End Function


'ファイル名を指示された方法で変換する
Private Function ChangedName(FileTitle1 As String, n As Integer) As String
    Dim fTi1 As String, fTi2 As String
    Dim fTp1 As String, fTp2 As String
    Dim srcTi As String, outTi As String
    Dim srcTp As String, outTp As String
    
    srcTi = Trim$(Text2.Text)
    srcTp = Trim$(Text5.Text)
    
    outTi = Trim$(Text3.Text)
    outTp = Trim$(Text6.Text)
    
    fTi1 = GetFileTitle(FileTitle1)
    fTp1 = GetFileType(FileTitle1)
  
    fTi2 = ChangedNameSp(srcTi, outTi, fTi1, n)
    If fTi2 = "" Then
        fTp2 = ""
    Else
        fTp2 = ChangedNameSp(srcTp, outTp, fTp1, n)
        If (outTp <> "") And (fTp2 = "") Then
            'ソース条件が不一致の場合
            fTi2 = ""
        End If
    End If
  
    ChangedName = fTi2 & IIf(fTp2 = "", "", "." & fTp2)
    
End Function

'テキストボックス内のデータのチェック
Private Sub CheckTextbox()
    
    Dim a1 As Integer, a2 As Integer
    Dim b1 As Integer, b2 As Integer
    Dim s As String, s1 As String, s2 As String
    Dim i As Integer
    
    Text2.Text = CheckTextIt(Text2.Text, 1) 'ファイルタイトル ソース条件
    Text3.Text = CheckTextIt(Text3.Text, 2) 'ファイルタイトル 変更書式
    Text5.Text = CheckTextIt(Text5.Text, 1) 'ファイル拡張子 ソース条件
    Text6.Text = CheckTextIt(Text6.Text, 2) 'ファイル拡張子 変更書式
    Text4.Text = CheckTextIt(Text4.Text, 3) '開始番号
    
    Text3.Text = CheckTextboxSp(Text2.Text, Text3.Text)
    Text6.Text = CheckTextboxSp(Text5.Text, Text6.Text)

End Sub

'ソース条件(Src1)と変更書式(Out1)の * , ? の数の違いに問題はないか調べる
Private Function CheckTextboxSp(Src1 As String, Out1) As String
    Dim a1 As Integer, a2 As Integer
    Dim b1 As Integer, b2 As Integer
    Dim s As String, s1 As String, s2 As String
    Dim i As Integer
   
    'ソース条件の * , ? の数をカウント
    a1 = 0
    b1 = 0
    s = Src1
    For i = 1 To Len(s)
        s1 = Mid$(s, i, 1)
        If s1 = "*" Then
            a1 = a1 + 1
        ElseIf s1 = "?" Then
            b1 = b1 + 1
        End If
    Next i
    
    '変更書式の * , ? の数と比較
    a2 = 0
    b2 = 0
    s2 = ""
    s = Out1
    For i = 1 To Len(s)
        s1 = Mid$(s, i, 1)
        If s1 = "*" Then
            a2 = a2 + 1
            If a2 > a1 Then
                s1 = ""
            End If
        ElseIf s1 = "?" Then
            b2 = b2 + 1
            If b2 > b1 Then
                s1 = ""
            End If
        End If
        s2 = s2 & s1
    Next i
    
    CheckTextboxSp = s2

End Function

'テキストボックスのデータの正常化
Private Function CheckTextIt(txt1 As String, m As Integer) As String
    Dim s1 As String, s2 As String
    Dim n As Integer, i As Integer, k As Integer
    Dim f As Integer, j As Integer
    Dim tmp1 As String, tmp2 As String
    Dim tmp3 As String, tmp4() As String
    
    '不正文字リストの選択
    Select Case m
    Case 1
        tmp1 = "\;:/""<>|"
    Case 2
        tmp1 = ";:/""<>|"
    End Select
    
    If m = 3 Then
        '開始番号のチェック
        n = Val(txt1)
        If n < 0 Then
          n = 0
        End If
        tmp2 = Format$(n)
    Else
        '書式のチェック
        tmp2 = ""
        n = Len(txt1)
        s1 = ""
        For i = 1 To n
            s2 = Mid$(txt1, i, 1)
            If (s1 = s2) And (s1 = "*") And (s2 = "*") And (m = 1) Then
                '*が２つ並んでいる
                s1 = s2
                s2 = ""
            ElseIf (InStr(1, tmp1, s2) > 0) Then
                '不正文字リストtmp1にある
                s2 = ""
            Else
                s1 = s2
            End If
            tmp2 = tmp2 & s2
        Next i
    End If
  
    If m = 2 Then
        'ナンバリング書式\1\〜\9\のチェック
        If InStr(1, tmp2, "\") > 0 Then
            '書式に\が含まれている
            n = Len(tmp2)
            tmp3 = ""
            If n < 3 Then
                '書式の文字数が３文字未満の時の処理
                For i = 1 To n
                    s1 = Mid$(tmp2, i, 1)
                    If s1 = "\" Then
                        s1 = ""
                    End If
                    tmp3 = tmp3 & s1
                Next i
            Else
                '書式の文字数が３文字以上の時の処理
                i = 1
                Do While (i < n - 1)
                    s1 = Mid$(tmp2, i, 1)
                    If s1 = "\" Then
                        s2 = Mid$(tmp2, i + 1, 1)
                        If InStr(1, "123456789", s2) > 0 Then
                            s2 = Mid$(tmp2, i + 2, 1)
                            If s2 = "\" Then
                                s1 = Mid$(tmp2, i, 3)
                                i = i + 2
                            Else
                                s1 = ""
                            End If
                        Else
                            s1 = ""
                        End If
                    End If
                    tmp3 = tmp3 & s1
                    i = i + 1
                Loop
                '右端の残った文字部分を走査
                Do While (i <= n)
                    s1 = Mid$(tmp2, i, 1)
                    If s1 = "\" Then
                        s1 = ""
                    End If
                    tmp3 = tmp3 & s1
                    i = i + 1
                Loop
            End If
            tmp2 = tmp3
        End If
    End If
    
    If m = 1 Then
        'srcの ? 記号を正しく使ってるかのチェック
        If tmp2 <> "" Then
            tmp4 = Wakeru(tmp2)
            k = UBound(tmp4) - 1
            For i = 1 To k
                tmp1 = tmp4(i)
                s1 = Mid$(tmp1, 1, 1)
                If s1 <> "*" Then
                    f = 0
                    For j = 1 To Len(tmp1)
                        s2 = Mid$(tmp1, j, 1)
                        If s2 <> "?" Then
                            f = 1
                            Exit For
                        End If
                    Next j
                    If f = 0 Then
                        tmp4(i) = " "
                    End If
                End If
            Next i
            tmp2 = Join(tmp4, "")
        End If
    End If
  
    CheckTextIt = Trim$(tmp2)

End Function

'コンボボックスの項目削除処理
Private Sub Combo1_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim n As Integer
    
    If (Shift = 1) And (KeyCode = vbKeyDelete) Then
        n = Combo1.ListIndex
        If n >= 0 Then
            Combo1.RemoveItem n
            Combo1.Refresh
        End If
    End If
End Sub

'コンボボックスでエンターキーを押した時
Private Sub Combo1_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Command6_Click
    End If
End Sub

'リストへ追加
Private Sub Command1_Click()
    Call File1ToListView1
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

'リストから削除
Private Sub Command2_Click()
    Dim i As Integer
    Dim c As Integer
       
    c = ListView1.ListItems.Count
       
    For i = c To 1 Step -1
        If ListView1.ListItems(i).Selected Then
            ListView1.ListItems.Remove i
        End If
    Next i
    
    ListView1.Refresh
    
    If ListView1.ListItems.Count < 1 Then
        Command7.Enabled = False
        Command9.Enabled = False
    End If
End Sub

'リストのクリア
Private Sub Command3_Click()
    ListView1.ListItems.Clear
    ListView1.Refresh
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

'一段上へ移動
Private Sub Command4_Click()
    Dim i As Integer, c As Integer, k As Integer
    Dim s As String, j As Integer, n As Integer
    
    n = ListView1.ColumnHeaders.Count - 1
    
    c = ListView1.ListItems.Count
    For i = 2 To c
        If ListView1.ListItems.Item(i).Selected Then
            k = i - 1
            With ListView1.ListItems
                s = .Item(i).Text
                .Item(i).Text = .Item(k).Text
                .Item(k).Text = s
                For j = 1 To n
                    s = .Item(i).SubItems(j)
                    .Item(i).SubItems(j) = .Item(k).SubItems(j)
                    .Item(k).SubItems(j) = s
                Next j
                .Item(i).Selected = False
                .Item(k).Selected = True
            End With
        End If
    Next i
  
End Sub

'一段下へ移動
Private Sub Command5_Click()
    Dim i As Integer, c As Integer, k As Integer
    Dim s As String, j As Integer, n As Integer
    
    n = ListView1.ColumnHeaders.Count - 1
    
    c = ListView1.ListItems.Count - 1
    For i = c To 1 Step -1
        If ListView1.ListItems.Item(i).Selected Then
            k = i + 1
            With ListView1.ListItems
                s = .Item(i).Text
                .Item(i).Text = .Item(k).Text
                .Item(k).Text = s
                For j = 1 To n
                    s = .Item(i).SubItems(j)
                    .Item(i).SubItems(j) = .Item(k).SubItems(j)
                    .Item(k).SubItems(j) = s
                Next j
                .Item(i).Selected = False
                .Item(k).Selected = True
            End With
        End If
    Next i

End Sub

'反映ボタンを押した時の処理
Private Sub Command6_Click()
    Dim i As Integer, s As String
    Dim k As Integer
    
    On Error GoTo Command6_Click_ERROR
    
    s = Combo1.Text
    
    File1.Pattern = s
    
    'コンボボックスに同じパターンがあるかチェック
    k = 0
    For i = 0 To Combo1.ListCount - 1
        If Combo1.List(i) = s Then
            k = 1
            Exit For
        End If
    Next i
    
    'コンボボックスにパターンを追加
    If k = 0 Then
        Combo1.AddItem s
    End If
    
    Exit Sub

Command6_Click_ERROR:
    MsgBox Error(Err), , Err
End Sub

'変換開始ボタンを押した時の処理
Private Sub Command7_Click()
  Dim i As Integer
  Dim n As Integer
  Dim s1 As String, s2 As String, s3 As String
      
  If MsgBox("変換を開始します。よろしいですか？", vbOKCancel, "最終確認") = vbCancel Then
    Exit Sub
  End If
      
  n = ListView1.ListItems.Count
  
  
  'ファイル名変換
  
    For i = 1 To n
        If ListView1.ListItems(i).SubItems(SB_RNAME) <> "" Then
            With ListView1.ListItems(i)
                s1 = .SubItems(SB_FPATH) & .Text
                s2 = .SubItems(SB_FPATH) & .SubItems(SB_RNAME)
            End With
#If REALRUN Then
            Name s1 As s2
#End If
            With ListView1.ListItems(i)
                s3 = .Text
                .Text = .SubItems(SB_RNAME)
                .SubItems(SB_RNAME) = s3
            End With
        End If
    Next i
  
    ListView1.ColumnHeaders(CH_RNAME).Text = "Before"
    ListView1.Refresh
    
    File1.Refresh
    Command7.Enabled = False
    Command9.Enabled = True

End Sub

'チェックボタンを押した時の処理
Private Sub Command8_Click()
    Dim i As Integer, j As Integer
    Dim n As Integer, m As Integer
    Dim s1 As String, s2 As String
    
    Call CheckTextbox
    
    If (Text2.Text = "") Or (Text3.Text = "") Then
        MsgBox "エラー", , "エラー"
        Exit Sub
    End If
      
    ListView1.ColumnHeaders(CH_RNAME).Text = "After"
        
    n = ListView1.ListItems.Count
    
    m = Val(Text4.Text)
  
  'ファイル名変換
  
  For i = 1 To n
    s1 = ListView1.ListItems(i).Text
    s2 = ChangedName(s1, m + i - 1)
    With ListView1.ListItems(i)
        .SubItems(SB_RNAME) = s2
        .SubItems(SB_ERROR) = ""
    End With
  Next i
  
  '重複チェック
  
  'チェック用リストの作成
  List1.Clear
  For i = 1 To n
    With ListView1.ListItems(i)
      s2 = .SubItems(SB_FPATH) & .SubItems(SB_RNAME)
    End With
    List1.AddItem s2
  Next i
  
  m = 0
  
    For i = 0 To n - 1
        If ListView1.ListItems(i + 1).SubItems(SB_RNAME) <> "" Then
  
            '元ファイル名リストと変更後ファイル名との重複チェック
            s1 = UCase$(List1.List(i))
            For j = 1 To n
                If (i + 1) <> j Then
                    With ListView1.ListItems(j)
                        s2 = .SubItems(SB_FPATH) & .Text
                    End With
                    s2 = UCase$(s2)
                    If s1 = s2 Then
                        ListView1.ListItems(i + 1).SubItems(SB_ERROR) = "*"
                        m = m + 1
                    End If
                End If
            Next j
            
            '変更後ファイル同士の重複チェック
            s1 = UCase$(List1.List(i))
            For j = 0 To n - 1
                If i <> j Then
                    s2 = UCase$(List1.List(j))
                    If s1 = s2 Then
                        ListView1.ListItems(i + 1).SubItems(SB_ERROR) = "*"
                        m = m + 1
                    End If
                End If
            Next j
            
            'リストにないファイル（同一フォルダ内のファイル）との重複チェック
            s1 = List1.List(i)
            If CheckFileExist(s1) Then
              ListView1.ListItems(i + 1).SubItems(SB_ERROR) = "*"
              m = m + 1
            End If
            
        End If
    Next i
    
    
    
    '重複がなかったら変換ボタンを有効にする
    If m = 0 Then
        For i = 1 To ListView1.ListItems.Count
            If ListView1.ListItems(i).SubItems(SB_RNAME) <> "" Then
                Command7.Enabled = True
                Exit For
            End If
        Next i
    Else
        Command7.Enabled = False
    End If
    
    Command9.Enabled = False
    ListView1.ColumnHeaders(CH_RNAME).Position = 2
    ListView1.ColumnHeaders(CH_ERROR).Position = 3
    ListView1.Refresh
End Sub

'ファイル名を元に戻す
Private Sub Command9_Click()
    Dim i As Integer
    Dim n As Integer
    Dim s1 As String, s2 As String, s3 As String
        
    If MsgBox("ファイル名を元に戻します。よろしいですか？", vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
        
    n = ListView1.ListItems.Count
      
    'ファイル名変換
    
    For i = 1 To n
        If ListView1.ListItems(i).SubItems(SB_RNAME) <> "" Then
        
            With ListView1.ListItems(i)
                s1 = .SubItems(SB_FPATH) & .Text
                s2 = .SubItems(SB_FPATH) & .SubItems(SB_RNAME)
            End With
        
#If REALRUN Then
        Name s1 As s2
#End If

            With ListView1.ListItems(i)
                s3 = .Text
                .Text = .SubItems(SB_RNAME)
                .SubItems(SB_RNAME) = s3
            End With
        End If
    Next i
  
    ListView1.ColumnHeaders(CH_RNAME).Text = "Before"
    ListView1.Refresh
    
    File1.Refresh
    Command7.Enabled = False
    Command9.Enabled = False

End Sub

'ディレクトリのチェンジ
Private Sub Dir1_Change()
    File1.Path = Dir1.Path
End Sub

'ＯＬＥドラッグドロップの処理(Dir1)
Private Sub Dir1_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Data.GetFormat(15) Then
        If GetAttr(Data.Files(1)) = vbDirectory Then
            Dir1.Path = Data.Files(1)
        End If
    End If
End Sub

'ＯＬＥドラッグオーバーの処理(Dir1)
Private Sub Dir1_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, State As Integer)
    If Data.GetFormat(15) Then
        If Data.Files.Count = 1 Then
            If GetAttr(Data.Files(1)) = vbDirectory Then
                Exit Sub
            End If
        End If
    End If
    Effect = 0
End Sub

'ドライブのチェンジ
Private Sub Drive1_Change()
    On Error GoTo Drive1_Change_Error
    
    Dir1.Path = Drive1.Drive
    
    Exit Sub
Drive1_Change_Error:
    
    MsgBox Error(Err), , "エラー"
    
    Drive1.Drive = StartDrive
    
End Sub

'ファイルリストのチェンジ
Private Sub File1_DblClick()
    Call File1ToListView1
End Sub

'ＯＬＥドラッグドロップの処理(File1)
Private Sub File1_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Data.GetFormat(15) Then
        If GetAttr(Data.Files(1)) = vbDirectory Then
            Dir1.Path = Data.Files(1)
        End If
    End If
End Sub

'ＯＬＥドラッグオーバーの処理(File1)
Private Sub File1_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, State As Integer)
    If Data.GetFormat(15) Then
        If Data.Files.Count = 1 Then
            If GetAttr(Data.Files(1)) = vbDirectory Then
                Exit Sub
            End If
        End If
    End If
    Effect = 0
End Sub

'フォームロード
Private Sub Form_Load()
    Dim i As Integer, n As Integer, s As String
    
    Label2.Caption = "説明" & vbCrLf _
                    & "\1\ 〜 \9\ ... ナンバリング。数字は桁数" & vbCrLf _
                    & "「*」,「?」は通常の使い方をする（未実装）"
                  
    Line1.X2 = Me.ScaleWidth
    Line2.X2 = Me.ScaleWidth
    
    StartDrive = Drive1.Drive
    
    '設定ファイル利用の初期化
    Call InitPPS
    
    'コンボボックスのパターンリストの読み出し
    n = getPtnListCount()
    If n > 0 Then
        For i = 1 To n
            s = getPtnList(i)
            Combo1.AddItem s
        Next i
    End If
    
    'お気に入りの読み出し
    n = getFvListCount()
    If n > 0 Then
        mnuFvList(0).Enabled = True
        For i = 1 To n
            s = getFvList(i)
            s = Left$(s, Len(s) - 1)
            If i > 1 Then
                Load mnuFvList(i - 1)
                mnuFvList(i - 1).Visible = True
            End If
            mnuFvList(i - 1).Caption = s
        Next i
        mnuDeleteFvList.Enabled = True
        mnuFavoritesClear.Enabled = True
        If n = 9 Then
            mnuAddFvList.Enabled = False
        End If
    End If

End Sub

'フォームアンロードクエリー時の処理
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim i As Integer, n As Integer, s As String
    
    'コンボボックスのパターンリストの保存
    n = Combo1.ListCount
    setPtnListCount n
    If n > 0 Then
        For i = 1 To n
            s = Combo1.List(i - 1)
            setPtnList i, s
        Next i
    End If
    
    'お気に入りの保存
    If mnuFvList(0).Enabled Then
        n = mnuFvList.Count
        setFvListCount n
        For i = 1 To n
            s = mnuFvList(i - 1).Caption
            s = s & ":"
            setFvList i, s
        Next i
    Else
        setFvListCount 0
    End If
    
End Sub

'リストビューの項目名をクリックした時の処理
Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Static n1 As Integer, n2 As Integer
    Dim n As Integer
    
    n = ColumnHeader.Index
    
    If n = CH_RNAME Then
        If ListView1.ColumnHeaders(CH_RNAME).Position <> CH_RNAME Then
            ListView1.ColumnHeaders(CH_ERROR).Position = CH_ERROR
            ListView1.ColumnHeaders(CH_RNAME).Position = CH_RNAME
            ListView1.Refresh
            Exit Sub
        End If
    End If
    
    Command7.Enabled = False
    Command9.Enabled = False
    
    If n = n1 Then
        If n2 = 1 Then
            ListView1.SortOrder = lvwDescending
            n2 = 0
        Else
            ListView1.SortOrder = lvwAscending
            n2 = 1
        End If
    Else
        n1 = n
        n2 = 1
        ListView1.SortOrder = lvwAscending
    End If
    
    ListView1.SortKey = n - 1
    ListView1.Sorted = True
    ListView1.Refresh
    ListView1.Sorted = False
  
End Sub

'ＯＬＥドラッグドロップの処理(ListView1)
Private Sub ListView1_OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    
    If Data.GetFormat(15) Then
        If GetAttr(Data.Files(1)) = vbDirectory Then
            Dir1.Path = Data.Files(1)
            For i = 0 To File1.ListCount - 1
                File1.Selected(i) = True
            Next i
            Call File1ToListView1
        End If
    End If
End Sub

'ＯＬＥドラッグオーバーの処理(ListView1)
Private Sub ListView1_OLEDragOver(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, State As Integer)
    If Data.GetFormat(15) Then
        If Data.Files.Count = 1 Then
            If GetAttr(Data.Files(1)) = vbDirectory Then
                Exit Sub
            End If
        End If
    End If
    Effect = 0
End Sub

'お気に入りに追加する
Private Sub mnuAddFvList_Click()
    Dim n As Integer, i As Integer
    Dim s As String, s1 As String
    
    Call CheckTextbox
    
    If (Text2.Text = "") Or (Text3.Text = "") Then
        MsgBox "エラー", , "エラー"
        Exit Sub
    End If
    
    n = mnuFvList.Count
    
    
    s = "    " & Text2.Text & " : " & Text5.Text & "    :  →" _
        & "  :    " & Text3.Text & " : " & Text6.Text
    Debug.Print s
    
    If MsgBox(s & vbCrLf & vbCrLf _
                    & "を追加します", vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    
    If mnuFvList(0).Enabled Then
        
        For i = 0 To n - 1
            s1 = mnuFvList(i).Caption
            s1 = Right$(s1, Len(s1) - 3)
            Debug.Print s1
            If s = s1 Then
                MsgBox "既に同じものが存在します", , "エラー"
                Exit Sub
            End If
        Next i
        
        Load mnuFvList(n)
        mnuFvList(n).Visible = True
        mnuFvList(n).Caption = "&" & (n + 1) & ":" & s
                
        If n = 8 Then
            mnuAddFvList.Enabled = False
        End If
        
    Else
        mnuFvList(0).Enabled = True
        mnuFvList(0).Caption = "&1:" & s
        
        mnuDeleteFvList.Enabled = True
        mnuFavoritesClear.Enabled = True
        
    End If

End Sub

'お気に入りから一つ削除する
Private Sub mnuDeleteFvList_Click()
    Dim a As Integer, n As Integer, i As Integer
    Dim s As String, s1 As String, s2 As String
    
    n = mnuFvList.Count
    
    s2 = "削除したい番号を入力して下さい"
    For i = 0 To n - 1
        s2 = s2 & vbCrLf & " " & mnuFvList(i).Caption
    Next i
    
    s = ""
    s = InputBox(s2)
    
    If s = "" Then
        MsgBox "キャンセルされました"
        Exit Sub
    End If
    
    a = CInt(Val(s))
    
    
    If (a < 1) Or (a > n) Then
        MsgBox "正しく入力して下さい", , "注意"
        Exit Sub
    End If
    
    s2 = mnuFvList(a - 1).Caption
    
    For i = a To n - 1
        s1 = mnuFvList(i).Caption
        s1 = Right$(s1, Len(s1) - 2)
        s1 = "&" & i & s1
        mnuFvList(i - 1).Caption = s1
    Next i
    
    If n > 1 Then
        Unload mnuFvList(n - 1)
    Else
        mnuFvList(0).Caption = "(ありません)"
        mnuFvList(0).Enabled = False
        
        mnuAddFvList.Enabled = True
        mnuDeleteFvList.Enabled = False
        mnuFavoritesClear.Enabled = False
        
    End If
    
    mnuAddFvList.Enabled = True

    MsgBox s2 & vbCrLf & "の削除に成功しました"
    
End Sub

'お気に入りを全削除する
Private Sub mnuFavoritesClear_Click()
    Dim n As Integer, i As Integer
    
    n = mnuFvList.Count - 1
    
    If MsgBox("お気に入りをすべて削除します", vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    
    If n > 0 Then
        For i = n To 1 Step -1
            Unload mnuFvList(i)
        Next i
    End If
    
    mnuFvList(0).Caption = "(ありません)"
    mnuFvList(0).Enabled = False
    
    mnuAddFvList.Enabled = True
    mnuDeleteFvList.Enabled = False
    mnuFavoritesClear.Enabled = False
    
    MsgBox "お気に入りをすべて削除しました", , "確認"

End Sub

'お気に入りからロード
Private Sub mnuFvList_Click(Index As Integer)
    Dim s As String
    Dim tmp1() As String
    
    s = mnuFvList(Index).Caption & ":"
    tmp1() = Split(s, ":")
    
    Text2.Text = Trim$(tmp1(1))
    Text3.Text = Trim$(tmp1(4))
    Text5.Text = Trim$(tmp1(2))
    Text6.Text = Trim$(tmp1(5))

End Sub

'メニュー「終了」クリック
Private Sub mnuQuitApplication_Click()
    Unload Me
End Sub

'リセットする
Private Sub mnuReset_Click()

    If MsgBox("全てリセットされます", vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    
    'Dir1.Path = CurDir$()
    File1.Pattern = "*.*"
    Combo1.Text = "*.*"
    Text2.Text = "*"
    Text3.Text = "*"
    Text4.Text = "1"
    Text5.Text = "*"
    Text6.Text = "*"
    'ListView1.ListItems.Clear
    
    Command7.Enabled = False
    Command9.Enabled = False

End Sub

'ソースファイルタイトル書式テキストボックスを編集したときの処理
Private Sub Text2_Change()
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

Private Sub Text2_LostFocus()
    Text2.Text = CheckTextIt(Text2.Text, 1)
End Sub

'変換ファイルタイトル書式用テキストボックスを編集した時の処理
Private Sub Text3_Change()
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

Private Sub Text3_LostFocus()
    Text3.Text = CheckTextIt(Text3.Text, 2)
    Text3.Text = CheckTextboxSp(Text2.Text, Text3.Text)
End Sub

'開始番号設定テキストボックスを編集した時の処理
Private Sub Text4_Change()
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

Private Sub Text4_LostFocus()
    Text4.Text = CheckTextIt(Text4.Text, 3)
End Sub

'ソースファイルタイトル書式用テキストボックスを編集した時の処理
Private Sub Text5_Change()
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

Private Sub Text5_LostFocus()
    Text5.Text = CheckTextIt(Text5.Text, 1)
End Sub

'変換ファイルタイトル書式用テキストボックスを編集した時の処理
Private Sub Text6_Change()
    Command7.Enabled = False
    Command9.Enabled = False
End Sub

Private Sub Text6_LostFocus()
    Text6.Text = CheckTextIt(Text6.Text, 2)
    Text6.Text = CheckTextboxSp(Text5.Text, Text6.Text)
End Sub
