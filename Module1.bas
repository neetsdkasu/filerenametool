Attribute VB_Name = "Module1"
Option Explicit

'ソース書式を解析分解する
Public Function Wakeru(Str1 As String) As String()
    Dim f As Integer, n As Integer
    Dim i As Integer
    Dim s As String, t As String
    Dim tmp1() As String
    
    i = 1
    f = 0
    t = ""
    
    Do While (i <= Len(Str1))
        
        s = Mid$(Str1, i, 1)
        
        If s = "*" Then
            
            If f = 2 Then
                t = t & s
            Else
                
                If f = 1 Then
                    ReDim Preserve tmp1(n)
                    tmp1(n) = t
                    n = n + 1
                End If
                ReDim Preserve tmp1(n)
                tmp1(n) = s
                n = n + 1
                f = 0
                t = ""
            End If
            
        ElseIf s = "\" Then
        
            If f = 1 Then
                ReDim Preserve tmp1(n)
                tmp1(n) = t
                n = n + 1
            End If
            
            If f = 2 Then
                ReDim Preserve tmp1(n)
                tmp1(n) = t & s
                n = n + 1
                f = 0
                t = ""
            Else
                t = s
                f = 2
            End If
            
        Else
        
            If f = 2 Then
                t = t & s
            Else
                t = t & s
                f = 1
            End If
            
        End If
        
        i = i + 1
    
    Loop
    
    If f > 0 Then
        ReDim Preserve tmp1(n)
        tmp1(n) = t
    End If
    
    Wakeru = tmp1

End Function

'ソース条件(src2)に従い、ファイル名(str1)を分割する
Public Function Wakeru2(Str1 As String, src2() As String) As String()
    Dim n As Integer, i As Integer, c As Integer, w As Integer, j As Integer
    Dim m As Integer, e As Integer, nn As Integer, k As Integer, f As Integer
    Dim p As Integer
    Dim s1 As String, s2 As String, s3 As String
    Dim tmp1() As String, tmp2 As String, tmp3 As String
    
    n = UBound(src2)
    
    e = 0
    
    ReDim tmp1(0)
        
    tmp1(0) = ""
    tmp3 = Str1
    m = Len(tmp3)
    c = 0
    nn = 0
    
    For i = 0 To n - 1
        tmp2 = src2(i)
        If tmp2 = "*" Then
            c = 1
        Else
            w = Len(tmp2)
            If m < w Then
                e = 1
                Exit For
            Else
                k = 0
                For j = 0 To m - w
                    f = 0
                    s3 = ""
                    For p = 1 To w
                        '部分探索開始
                        s1 = Mid$(tmp2, p, 1)
                        s2 = Mid$(tmp3, j + p, 1)
                        If s1 <> "?" Then
                            If s1 <> s2 Then
                                '不一致
                                f = 2
                                Exit For
                            End If
                        Else
                            s3 = s3 & s2
                        End If
                    Next p
                    If f = 0 Then
                        '一致箇所発見
                        tmp1(0) = tmp1(0) & s3
                        If (j > 0) And (c = 1) Then
                            '手前に * があったら
                            nn = nn + 1
                            ReDim Preserve tmp1(nn)
                            tmp1(nn) = Left$(tmp3, j)
                        End If
                        tmp3 = Right$(tmp3, m - (j + w))
                        m = Len(tmp3)
                        k = 1
                        Exit For
                    End If
                Next j
                If k = 0 Then
                    '条件不一致
                    e = 2
                    Exit For
                End If
                c = 0
            End If
        End If
    Next i
    
    '一番最後のソース条件確認
    If src2(n) = "*" Then
        c = 0
        nn = nn + 1
        ReDim Preserve tmp1(nn)
        tmp1(nn) = tmp3
    Else
        tmp2 = src2(n)
        w = Len(tmp2)
        If m < w Then
            e = 3
        Else
            f = 0
            s3 = ""
            For i = 1 To w
                s1 = Mid$(tmp2, w - i + 1, 1)
                s2 = Mid$(tmp3, m - i + 1, 1)
                If s1 <> "?" Then
                    If s1 <> s2 Then
                        '一致なし
                        f = 2
                        e = 4
                        Exit For
                    End If
                Else
                    s3 = s2 & s3
                End If
            Next i
            If f = 0 Then
                '一致発見
                tmp1(0) = tmp1(0) & s3
                If c = 1 Then
                    nn = nn + 1
                    ReDim Preserve tmp1(nn)
                    tmp1(nn) = Left$(tmp3, m - w)
                End If
            End If
        End If
    End If
    
    If e > 0 Then
        tmp1(0) = "**"
    End If
    
    Wakeru2 = tmp1
    
End Function

'ファイルの拡張子を取得する
Public Function GetFileType(FileTitle1 As String) As String
  Dim s As String
  Dim n As Integer
  
  s = StrReverse(FileTitle1)
  n = InStr(1, s, ".") - 1
  If n > 0 Then
    GetFileType = LCase$(Right$(FileTitle1, n))
  Else
    GetFileType = ""
  End If

End Function

'ファイル名を取得する
Public Function GetFileTitle(FileTitle1 As String) As String
  Dim s As String
  Dim n As Integer
  
  s = StrReverse(FileTitle1)
  n = InStr(1, s, ".")
  If n > 0 Then
    GetFileTitle = Left$(FileTitle1, Len(s) - n)
  Else
    GetFileTitle = FileTitle1
  End If

End Function

'ファイルが存在しているか確認する
Public Function CheckFileExist(FilePath1 As String) As Boolean
  On Error GoTo CheckFileExist_ERROR
  
  FileLen FilePath1
  
  CheckFileExist = True
  
  Exit Function

CheckFileExist_ERROR:
  
  CheckFileExist = False

End Function






